// Ceres Solver - A fast non-linear least squares minimizer
// Copyright 2015 Google Inc. All rights reserved.
// http://ceres-solver.org/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: sameeragarwal@google.com (Sameer Agarwal)
//
// Templated struct implementing the camera model and residual
// computation for bundle adjustment used by Noah Snavely's Bundler
// SfM system. This is also the camera model/residual for the bundle
// adjustment problems in the BAL dataset. It is templated so that we
// can use Ceres's automatic differentiation to compute analytic
// jacobians.
//
// For details see: http://phototour.cs.washington.edu/bundler/
// and http://grail.cs.washington.edu/projects/bal/

#ifndef CERES_EXAMPLES_SNAVELY_REPROJECTION_ERROR_H_
#define CERES_EXAMPLES_SNAVELY_REPROJECTION_ERROR_H_

#include "ceres/rotation.h"
#include <algorithm>

namespace ceres {
namespace examples {

// Templated pinhole camera model for used with Ceres.  The camera is
// parameterized using 9 parameters: 3 for rotation, 3 for translation, 1 for
// focal length and 2 for radial distortion. The principal point is not modeled
// (i.e. it is assumed be located at the image center).
struct SnavelyReprojectionError {
  SnavelyReprojectionError(double observed_x, double observed_y)
      : observed_x(observed_x), observed_y(observed_y) {}

  template <typename T>
  bool operator()(const T* const camera,
                  const T* const pointXY,
                  const T* const pointZ,
                  T* residuals) const {

    
    const T Txi = camera[3];
    const T Tyi = camera[4];
    const T Tzi = camera[5];
    const T Rxi = camera[0];
    const T Ryi = camera[1];
    const T Rzi = camera[2];
    // const T& l1 = camera[7];
    // const T& l2 = camera[8];
    // const T focal = camera[6];


    const T xj = pointXY[0] / pointZ[0];
    const T yj = pointXY[1] / pointZ[0];
    const T wj = T(1) / pointZ[0];
    // normalized
//    const T pxij = T(observed_x) / T(2720.00 / 3264 * 2);
//    const T pyij = T(observed_y) / T(2720.00 / 3264 * 2);

    // fisher normalized
   // const T pxij = T(observed_x) / T(1781.00 / 1920 * 2);
   // const T pyij = T(observed_y) / T(1781.00 / 1920 * 2);
   const T pxij = T(observed_x) / T(1781.00);
   const T pyij = T(observed_y) / T(1781.00);

    // iphone
    // const T pxij = T(observed_x) / T(2720.00);
    // const T pyij = T(observed_y) / T(2720.00);
    
    const T axij = xj - Rzi * yj + Ryi;
    const T bxij = Txi;
    const T ayij = yj - Rxi + Rzi * xj;
    const T byij = Tyi;
    const T cij = -Ryi * xj + Rxi * yj + T(1);
    const T dij = Tzi;
    const T exij = pxij * cij - axij;
    const T fxij = pxij * dij - bxij;
    const T eyij = pyij * cij - ayij;
    const T fyij = pyij * dij - byij;

    // std::cout << cij << "-> cij\n";
    // std::cout << dij << "-> dij\n";

    // std::cout << observed_x << "-> ox\n";
    // std::cout << observed_y << "-> oy\n";
    // std::cout << Txi << "-> txy\n";
    // std::cout << Tyi << "-> tyi\n";
    // std::cout << Tzi << "-> tzi\n";
    // std::cout << point[0] << "-> point 0\n";
    // std::cout << point[1] << "-> point 1\n";
    // std::cout << point[2] << "-> point 2\n";

    residuals[0] = T((exij + fxij * wj) / (cij + dij * wj));
    residuals[1] = T((eyij + fyij * wj) / (cij + dij * wj));

    // std::cout << T((exij + fxij * wj) / (cij + dij * wj)) << "\n";
    // std::cout << T((eyij + fyij * wj) / (cij + dij * wj)) << "\n";

    return true;
  }

  // Factory to hide the construction of the CostFunction object from
  // the client code.
  static ceres::CostFunction* Create(const double observed_x,
                                     const double observed_y) {
    return (new ceres::AutoDiffCostFunction<SnavelyReprojectionError, 2, 9, 2, 1>(
                new SnavelyReprojectionError(observed_x, observed_y)));
  }

  double observed_x;
  double observed_y;
};

struct SnavelyReprojectionErrorFirst {
  SnavelyReprojectionErrorFirst(double observed_x, double observed_y)
      : observed_x(observed_x), observed_y(observed_y) {}

  template <typename T>
  bool operator()(const T* const camera,
                  const T* const pointXY,
                  const T* const pointZ,
                  T* residuals) const {

    
    const T Txi = std::min(T(0.5), camera[3]);
    const T Tyi = std::min(T(0.5), camera[4]);
    const T Tzi = std::min(T(0.5), camera[5]);
    const T Rxi = std::min(T(0.5), camera[0]);
    const T Ryi = std::min(T(0.5), camera[1]);
    const T Rzi = std::min(T(0.5), camera[2]);
    // const T& l1 = camera[7];
    // const T& l2 = camera[8];
    // const T focal = camera[6];


//     const T xj = pointXY[0] / pointZ[0];
//     const T yj = pointXY[1] / pointZ[0];
//     const T wj = T(1) / pointZ[0];
//     // normalized
// //    const T pxij = T(observed_x) / T(2720.00 / 3264 * 2);
// //    const T pyij = T(observed_y) / T(2720.00 / 3264 * 2);

//     // fisher normalized
//    // const T pxij = T(observed_x) / T(1781.00 / 1920 * 2);
//    // const T pyij = T(observed_y) / T(1781.00 / 1920 * 2);
//    const T pxij = T(observed_x) / T(1781.00);
//    const T pyij = T(observed_y) / T(1781.00);

//     // iphone
//     // const T pxij = T(observed_x) / T(2720.00);
//     // const T pyij = T(observed_y) / T(2720.00);
    
//     const T axij = xj - Rzi * yj + Ryi;
//     const T bxij = Txi;
//     const T ayij = yj - Rxi + Rzi * xj;
//     const T byij = Tyi;
//     const T cij = -Ryi * xj + Rxi * yj + T(1);
//     const T dij = Tzi;
//     const T exij = pxij * cij - axij;
//     const T fxij = pxij * dij - bxij;
//     const T eyij = pyij * cij - ayij;
//     const T fyij = pyij * dij - byij;

    // std::cout << cij << "-> cij\n";
    // std::cout << dij << "-> dij\n";

    // std::cout << observed_x << "-> ox\n";
    // std::cout << observed_y << "-> oy\n";
    // std::cout << Txi << "-> txy\n";
    // std::cout << Tyi << "-> tyi\n";
    // std::cout << Tzi << "-> tzi\n";
    // std::cout << point[0] << "-> point 0\n";
    // std::cout << point[1] << "-> point 1\n";
    // std::cout << point[2] << "-> point 2\n";

    residuals[0] = T((Txi*Txi + Tyi*Tyi  + Tzi*Tzi));
    residuals[1] = T((Rxi*Rxi + Ryi*Ryi + Rzi*Rzi));

    // std::cout << T((exij + fxij * wj) / (cij + dij * wj)) << "\n";
    // std::cout << T((eyij + fyij * wj) / (cij + dij * wj)) << "\n";

    return true;
  }

  // Factory to hide the construction of the CostFunction object from
  // the client code.
  static ceres::CostFunction* Create(const double observed_x,
                                     const double observed_y) {
    return (new ceres::AutoDiffCostFunction<SnavelyReprojectionErrorFirst, 2, 9, 2, 1>(
                new SnavelyReprojectionErrorFirst(observed_x, observed_y)));
  }

  double observed_x;
  double observed_y;
};


// Templated pinhole camera model for used with Ceres.  The camera is
// parameterized using 10 parameters. 4 for rotation, 3 for
// translation, 1 for focal length and 2 for radial distortion. The
// principal point is not modeled (i.e. it is assumed be located at
// the image center).
struct SnavelyReprojectionErrorWithQuaternions {
  // (u, v): the position of the observation with respect to the image
  // center point.
  SnavelyReprojectionErrorWithQuaternions(double observed_x, double observed_y)
      : observed_x(observed_x), observed_y(observed_y) {}

  template <typename T>
  bool operator()(const T* const camera_rotation,
                  const T* const camera_translation_and_intrinsics,
                  const T* const point,
                  T* residuals) const {
    const T& focal = camera_translation_and_intrinsics[3];
    const T& l1 = camera_translation_and_intrinsics[4];
    const T& l2 = camera_translation_and_intrinsics[5];

    // Use a quaternion rotation that doesn't assume the quaternion is
    // normalized, since one of the ways to run the bundler is to let Ceres
    // optimize all 4 quaternion parameters unconstrained.
    T p[3];
    QuaternionRotatePoint(camera_rotation, point, p);

    p[0] += camera_translation_and_intrinsics[0];
    p[1] += camera_translation_and_intrinsics[1];
    p[2] += camera_translation_and_intrinsics[2];

    // Compute the center of distortion. The sign change comes from
    // the camera model that Noah Snavely's Bundler assumes, whereby
    // the camera coordinate system has a negative z axis.
    T xp = - p[0] / p[2];
    T yp = - p[1] / p[2];

    // Apply second and fourth order radial distortion.
    T r2 = xp*xp + yp*yp;
    T distortion = T(1.0) + r2  * (l1 + l2  * r2);

    // Compute final projected point position.
    T predicted_x = focal * distortion * xp;
    T predicted_y = focal * distortion * yp;

    // The error is the difference between the predicted and observed position.
    residuals[0] = predicted_x - T(observed_x);
    residuals[1] = predicted_y - T(observed_y);

    return true;
  }

  // Factory to hide the construction of the CostFunction object from
  // the client code.
  static ceres::CostFunction* Create(const double observed_x,
                                     const double observed_y) {
    return (new ceres::AutoDiffCostFunction<
            SnavelyReprojectionErrorWithQuaternions, 2, 4, 6, 3>(
                new SnavelyReprojectionErrorWithQuaternions(observed_x,
                                                            observed_y)));
  }

  double observed_x;
  double observed_y;
};

}  // namespace examples
}  // namespace ceres

#endif  // CERES_EXAMPLES_SNAVELY_REPROJECTION_ERROR_H_
