# Install script for directory: /media/yiwen/Data/ceres-solver

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ceres" TYPE FILE FILES
    "/media/yiwen/Data/ceres-solver/include/ceres/autodiff_local_parameterization.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/rotation.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/numeric_diff_cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/autodiff_cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/conditioned_cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/types.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/gradient_problem.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/iteration_callback.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/dynamic_cost_function_to_functor.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/gradient_problem_solver.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/gradient_checker.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/c_api.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/cost_function_to_functor.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/local_parameterization.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/numeric_diff_options.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/normal_prior.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/sized_cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/dynamic_autodiff_cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/covariance.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/ceres.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/solver.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/problem.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/fpclassify.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/cubic_interpolation.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/version.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/loss_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/dynamic_numeric_diff_cost_function.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/ordered_groups.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/jet.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/crs_matrix.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ceres/internal" TYPE FILE FILES
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/macros.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/reenable_warnings.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/manual_constructor.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/autodiff.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/disable_warnings.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/fixed_array.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/eigen.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/port.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/numeric_diff.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/variadic_evaluate.h"
    "/media/yiwen/Data/ceres-solver/include/ceres/internal/scoped_ptr.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ceres/internal" TYPE FILE FILES "/media/yiwen/Data/ceres-solver/ceres-bin/config/ceres/internal/config.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/Ceres/CeresTargets.cmake")
    FILE(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/Ceres/CeresTargets.cmake"
         "/media/yiwen/Data/ceres-solver/ceres-bin/CMakeFiles/Export/share/Ceres/CeresTargets.cmake")
    IF(EXPORT_FILE_CHANGED)
      FILE(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/Ceres/CeresTargets-*.cmake")
      IF(OLD_CONFIG_FILES)
        MESSAGE(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/Ceres/CeresTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        FILE(REMOVE ${OLD_CONFIG_FILES})
      ENDIF(OLD_CONFIG_FILES)
    ENDIF(EXPORT_FILE_CHANGED)
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/Ceres" TYPE FILE FILES "/media/yiwen/Data/ceres-solver/ceres-bin/CMakeFiles/Export/share/Ceres/CeresTargets.cmake")
  IF("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/Ceres" TYPE FILE FILES "/media/yiwen/Data/ceres-solver/ceres-bin/CMakeFiles/Export/share/Ceres/CeresTargets-release.cmake")
  ENDIF("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/Ceres" TYPE FILE RENAME "CeresConfig.cmake" FILES "/media/yiwen/Data/ceres-solver/ceres-bin/CeresConfig-install.cmake")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/Ceres" TYPE FILE FILES
    "/media/yiwen/Data/ceres-solver/ceres-bin/CeresConfigVersion.cmake"
    "/media/yiwen/Data/ceres-solver/cmake/FindEigen.cmake"
    "/media/yiwen/Data/ceres-solver/cmake/FindGlog.cmake"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres/cmake_install.cmake")
  INCLUDE("/media/yiwen/Data/ceres-solver/ceres-bin/examples/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/media/yiwen/Data/ceres-solver/ceres-bin/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/media/yiwen/Data/ceres-solver/ceres-bin/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
