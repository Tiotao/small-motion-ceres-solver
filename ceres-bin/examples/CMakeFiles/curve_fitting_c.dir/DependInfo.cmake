# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/media/yiwen/Data/ceres-solver/examples/curve_fitting.c" "/media/yiwen/Data/ceres-solver/ceres-bin/examples/CMakeFiles/curve_fitting_c.dir/curve_fitting.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "CERES_GFLAGS_NAMESPACE=google"
  "EIGEN_MPL2_ONLY"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres/CMakeFiles/ceres.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "config"
  "../include"
  "../internal"
  "../internal/ceres"
  "/usr/include/eigen3"
  "/usr/include/suitesparse"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
