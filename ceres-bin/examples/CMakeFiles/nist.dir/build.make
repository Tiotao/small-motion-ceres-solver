# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /media/yiwen/Data/ceres-solver

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /media/yiwen/Data/ceres-solver/ceres-bin

# Include any dependencies generated for this target.
include examples/CMakeFiles/nist.dir/depend.make

# Include the progress variables for this target.
include examples/CMakeFiles/nist.dir/progress.make

# Include the compile flags for this target's objects.
include examples/CMakeFiles/nist.dir/flags.make

examples/CMakeFiles/nist.dir/nist.cc.o: examples/CMakeFiles/nist.dir/flags.make
examples/CMakeFiles/nist.dir/nist.cc.o: ../examples/nist.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /media/yiwen/Data/ceres-solver/ceres-bin/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/CMakeFiles/nist.dir/nist.cc.o"
	cd /media/yiwen/Data/ceres-solver/ceres-bin/examples && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/nist.dir/nist.cc.o -c /media/yiwen/Data/ceres-solver/examples/nist.cc

examples/CMakeFiles/nist.dir/nist.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/nist.dir/nist.cc.i"
	cd /media/yiwen/Data/ceres-solver/ceres-bin/examples && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /media/yiwen/Data/ceres-solver/examples/nist.cc > CMakeFiles/nist.dir/nist.cc.i

examples/CMakeFiles/nist.dir/nist.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/nist.dir/nist.cc.s"
	cd /media/yiwen/Data/ceres-solver/ceres-bin/examples && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /media/yiwen/Data/ceres-solver/examples/nist.cc -o CMakeFiles/nist.dir/nist.cc.s

examples/CMakeFiles/nist.dir/nist.cc.o.requires:
.PHONY : examples/CMakeFiles/nist.dir/nist.cc.o.requires

examples/CMakeFiles/nist.dir/nist.cc.o.provides: examples/CMakeFiles/nist.dir/nist.cc.o.requires
	$(MAKE) -f examples/CMakeFiles/nist.dir/build.make examples/CMakeFiles/nist.dir/nist.cc.o.provides.build
.PHONY : examples/CMakeFiles/nist.dir/nist.cc.o.provides

examples/CMakeFiles/nist.dir/nist.cc.o.provides.build: examples/CMakeFiles/nist.dir/nist.cc.o

# Object files for target nist
nist_OBJECTS = \
"CMakeFiles/nist.dir/nist.cc.o"

# External object files for target nist
nist_EXTERNAL_OBJECTS =

bin/nist: examples/CMakeFiles/nist.dir/nist.cc.o
bin/nist: examples/CMakeFiles/nist.dir/build.make
bin/nist: lib/libceres.a
bin/nist: /usr/lib/x86_64-linux-gnu/libgflags.so
bin/nist: /usr/lib/x86_64-linux-gnu/libglog.so
bin/nist: /usr/lib/x86_64-linux-gnu/libspqr.so
bin/nist: /usr/lib/libtbb.so
bin/nist: /usr/lib/libtbbmalloc.so
bin/nist: /usr/lib/x86_64-linux-gnu/libcholmod.so
bin/nist: /usr/lib/x86_64-linux-gnu/libccolamd.so
bin/nist: /usr/lib/x86_64-linux-gnu/libcamd.so
bin/nist: /usr/lib/x86_64-linux-gnu/libcolamd.so
bin/nist: /usr/lib/x86_64-linux-gnu/libamd.so
bin/nist: /usr/lib/liblapack.so
bin/nist: /usr/lib/libf77blas.so
bin/nist: /usr/lib/libatlas.so
bin/nist: /usr/lib/x86_64-linux-gnu/libsuitesparseconfig.a
bin/nist: /usr/lib/x86_64-linux-gnu/librt.so
bin/nist: /usr/lib/liblapack.so
bin/nist: /usr/lib/libf77blas.so
bin/nist: /usr/lib/libatlas.so
bin/nist: /usr/lib/x86_64-linux-gnu/libsuitesparseconfig.a
bin/nist: /usr/lib/x86_64-linux-gnu/librt.so
bin/nist: examples/CMakeFiles/nist.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../bin/nist"
	cd /media/yiwen/Data/ceres-solver/ceres-bin/examples && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/nist.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/CMakeFiles/nist.dir/build: bin/nist
.PHONY : examples/CMakeFiles/nist.dir/build

examples/CMakeFiles/nist.dir/requires: examples/CMakeFiles/nist.dir/nist.cc.o.requires
.PHONY : examples/CMakeFiles/nist.dir/requires

examples/CMakeFiles/nist.dir/clean:
	cd /media/yiwen/Data/ceres-solver/ceres-bin/examples && $(CMAKE_COMMAND) -P CMakeFiles/nist.dir/cmake_clean.cmake
.PHONY : examples/CMakeFiles/nist.dir/clean

examples/CMakeFiles/nist.dir/depend:
	cd /media/yiwen/Data/ceres-solver/ceres-bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /media/yiwen/Data/ceres-solver /media/yiwen/Data/ceres-solver/examples /media/yiwen/Data/ceres-solver/ceres-bin /media/yiwen/Data/ceres-solver/ceres-bin/examples /media/yiwen/Data/ceres-solver/ceres-bin/examples/CMakeFiles/nist.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/CMakeFiles/nist.dir/depend

