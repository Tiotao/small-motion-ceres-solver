# CMake generated Testfile for 
# Source directory: /media/yiwen/Data/ceres-solver
# Build directory: /media/yiwen/Data/ceres-solver/ceres-bin
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(internal/ceres)
SUBDIRS(examples)
