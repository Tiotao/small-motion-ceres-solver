# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/media/yiwen/Data/ceres-solver/internal/ceres/compressed_col_sparse_matrix_utils_test.cc" "/media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres/CMakeFiles/compressed_col_sparse_matrix_utils_test.dir/compressed_col_sparse_matrix_utils_test.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "CERES_GFLAGS_NAMESPACE=google"
  "CERES_SUITESPARSE_VERSION=\"4.2.1\""
  "EIGEN_MPL2_ONLY"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres/CMakeFiles/test_util.dir/DependInfo.cmake"
  "/media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres/CMakeFiles/ceres.dir/DependInfo.cmake"
  "/media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "config"
  "../include"
  "../internal"
  "../internal/ceres"
  "/usr/include/eigen3"
  "/usr/include/suitesparse"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
