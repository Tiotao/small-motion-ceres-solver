# CMake generated Testfile for 
# Source directory: /media/yiwen/Data/ceres-solver/internal/ceres
# Build directory: /media/yiwen/Data/ceres-solver/ceres-bin/internal/ceres
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(array_utils_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/array_utils_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(autodiff_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/autodiff_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(autodiff_cost_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/autodiff_cost_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(autodiff_local_parameterization_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/autodiff_local_parameterization_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(block_jacobi_preconditioner_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/block_jacobi_preconditioner_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(block_random_access_dense_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/block_random_access_dense_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(block_random_access_diagonal_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/block_random_access_diagonal_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(block_random_access_sparse_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/block_random_access_sparse_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(block_sparse_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/block_sparse_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(bundle_adjustment_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/bundle_adjustment_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(c_api_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/c_api_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(canonical_views_clustering_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/canonical_views_clustering_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(compressed_row_sparse_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/compressed_row_sparse_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(conditioned_cost_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/conditioned_cost_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(corrector_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/corrector_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(cost_function_to_functor_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/cost_function_to_functor_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(covariance_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/covariance_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(cubic_interpolation_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/cubic_interpolation_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(detect_structure_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/detect_structure_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(dense_sparse_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/dense_sparse_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(dynamic_autodiff_cost_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/dynamic_autodiff_cost_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(dynamic_compressed_row_sparse_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/dynamic_compressed_row_sparse_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(dynamic_numeric_diff_cost_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/dynamic_numeric_diff_cost_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(evaluator_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/evaluator_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(gradient_checker_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/gradient_checker_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(gradient_checking_cost_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/gradient_checking_cost_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(gradient_problem_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/gradient_problem_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(gradient_problem_solver_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/gradient_problem_solver_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(graph_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/graph_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(graph_algorithms_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/graph_algorithms_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(householder_vector_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/householder_vector_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(implicit_schur_complement_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/implicit_schur_complement_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(iterative_schur_complement_solver_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/iterative_schur_complement_solver_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(jet_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/jet_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(levenberg_marquardt_strategy_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/levenberg_marquardt_strategy_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(dogleg_strategy_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/dogleg_strategy_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(line_search_preprocessor_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/line_search_preprocessor_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(local_parameterization_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/local_parameterization_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(loss_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/loss_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(minimizer_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/minimizer_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(normal_prior_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/normal_prior_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(numeric_diff_cost_function_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/numeric_diff_cost_function_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(ordered_groups_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/ordered_groups_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(parameter_block_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/parameter_block_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(parameter_block_ordering_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/parameter_block_ordering_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(partitioned_matrix_view_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/partitioned_matrix_view_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(polynomial_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/polynomial_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(problem_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/problem_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(program_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/program_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(reorder_program_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/reorder_program_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(residual_block_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/residual_block_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(residual_block_utils_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/residual_block_utils_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(rotation_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/rotation_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(schur_complement_solver_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/schur_complement_solver_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(schur_eliminator_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/schur_eliminator_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(single_linkage_clustering_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/single_linkage_clustering_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(small_blas_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/small_blas_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(solver_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/solver_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(compressed_col_sparse_matrix_utils_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/compressed_col_sparse_matrix_utils_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(symmetric_linear_solver_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/symmetric_linear_solver_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(triplet_sparse_matrix_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/triplet_sparse_matrix_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(trust_region_minimizer_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/trust_region_minimizer_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(trust_region_preprocessor_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/trust_region_preprocessor_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(unsymmetric_linear_solver_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/unsymmetric_linear_solver_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(visibility_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/visibility_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(visibility_based_preconditioner_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/visibility_based_preconditioner_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
ADD_TEST(system_test "/media/yiwen/Data/ceres-solver/ceres-bin/bin/system_test" "--test_srcdir" "/media/yiwen/Data/ceres-solver/data")
