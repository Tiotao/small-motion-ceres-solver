class Observation:
	obsIdCount = 0

	def __init__(self, position, camera, point):
		self.u = position[0]
		self.v = position[1]
		self.id = Observation.obsIdCount
		self.camera = camera
		self.point = point
		self.normal = [0,0,0]
		Observation.obsIdCount = Observation.obsIdCount + 1

	@staticmethod
	def count():
		return Observation.obsIdCount

	@staticmethod
	def setIdCount(n):
		Observation.obsIdCount = n

	def getId(self):
		return self.id

	def setId(self, id):
		self.id = id

	def getU(self):
		return self.u

	def getV(self):
		return self.v

	def getCamera(self):
		return self.camera

	def getPoint(self):
		return self.point

	def getPosition(self):
		return [self.u, self.v]

	def setU(self, u):
		self.u = u

	def setV(self, v):
		self.v = v

	def setPosition(self, position):
		self.setU(position[0])
		self.setV(position[1])

	def toString(self):
		string = "%r %r %r %r" %(self.getCamera().getId(), self.getPoint().getId(), self.getU(), self.getV())
		return string