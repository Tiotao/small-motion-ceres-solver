FILE = 'input.txt.ply'
COLOR_FILE = 'box-color.txt'

f = open(COLOR_FILE)
color_lines = f.readlines()


with open(NEW_FILE,'w') as new_file:
	with open(FILE) as old_file:
		line_number = 0
		point_number = 0
		for line in old_file:
			line_number = line_number + 1
			if line_number <= 10:
				new_file.write(line)
			else:
				old_line_array = line.split()
				if old_line_array[3] is '0' and  old_line_array[4] is '255' and old_line_array[5] is '0':
					new_file.write(line)
				else:
					color_array = color_lines[point_number].split()
					old_line_array[3] = color_array[0]
					old_line_array[4] = color_array[1]
					old_line_array[5] = color_array[2]
					new_line = " ".join(old_line_array)


