import subprocess
import shutil
from Problem import Problem

FILE = 'rubik.txt'

shutil.copy2('../new_data/rubik.txt', '../new_data/input.txt')

# problem = Problem()
# problem.load('input.txt')
# problem.removeInvalidPt()
# problem.write('output.txt')

# problem2 = Problem()
# problem2.removeInvalidPt()
# problem2.load('output.txt')
# problem2.write('output2.txt')

returnCode = 0

while returnCode == 0:
	returnCode = subprocess.call(['bin/bundle_adjuster', '--input=../new_data/input.txt', '--num_iterations=100', '--inner_iterations=true', '--nonmonotonic_steps=false'])
	# returnCode = subprocess.call(['bin/bundle_adjuster', '--input=../new_data/input.txt', '--logtostderr'])
	problem = Problem()
	problem.load('../new_data/input.txt')
	problem.removeInvalidPt()
	problem.write('../new_data/input.txt')
	del problem



