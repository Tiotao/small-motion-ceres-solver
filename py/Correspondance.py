from Point import Point

class Correspondance:
	def __init__(self, point, imageLoc, color):
		self.point = point
		self.imageLoc = imageLoc
		self.color = color

	def getId(self):
		return self.point.getId()

	def getPosition(self):
		return self.point.getPosition()

	def getImageLoc(self):
		return self.imageLoc

	def getColor(self):
		return self.color

