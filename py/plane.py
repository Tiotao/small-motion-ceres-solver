from Problem import Problem
from Point import Point
import random

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import math
import cv2
import cv
import os

IMAGE_WIDTH = 1920
IMAGE_HEIGHT = 1080

problem = Problem()
problem.load('../new_data/input.txt')
ptNum = len(problem.pts)
initImageObservation = problem.obs[:ptNum]
correspondance = []

image = cv2.imread('image.png')
def readNormal(filename):
	with open(filename, 'r') as f:
		content = f.readlines()[17:]
	normalArray = [map(float, c.split(' ')[3:6]) for c in content]
	print len(normalArray)
	return normalArray

normalArray = readNormal('../new_data/Success/8/output-normal.txt.ply')

def unit_vector(vector):
	""" Returns the unit vector of the vector.  """
	return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
	""" Returns the angle in radians between vectors 'v1' and 'v2'::

			>>> angle_between((1, 0, 0), (0, 1, 0))
			1.5707963267948966
			>>> angle_between((1, 0, 0), (1, 0, 0))
			0.0
			>>> angle_between((1, 0, 0), (-1, 0, 0))
			3.141592653589793
	"""
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	angle = np.arccos(np.dot(v1_u, v2_u))
	if np.isnan(angle):
		if (v1_u == v2_u).all():
			return 0.0
		else:
			return np.pi
	return angle
	

def getImageColor(image, u, v):
	raw = image[u][v]
	real = [image[u][v][2], image[u][v][1], image[u][v][0]]
	return np.array(real)

for index, obs in enumerate(initImageObservation):
	color = getImageColor(image, obs.getU(), obs.getV())
	correspondance.append((obs, color, normalArray[index]))


# find a point
# 	find its nearby point
# 		if distance close
# 			include point





def getDistance(a, b):
	return  math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)

def getColorDistance(a, b):
	distance = np.linalg.norm(a-b)
	return distance

def getStructureDistance(a, b):
	diff =  np.array(a.point.getPosition())-np.array(b.point.getPosition())
	distance = np.linalg.norm(diff)
	return distance

def getRandomInt(start, end):
	return random.randint(start, end) 

def getRandomColor():
	return (getRandomInt(0, 255), getRandomInt(0, 255), getRandomInt(0, 255))

def findNearbyObs(current, maxDistance, maxColorDistance, maxStructureDistance):
	plane = []
	for corr in correspondance:
		distance = getDistance(corr[0].getPosition(), current[0].getPosition())
		colorDistance = getColorDistance(corr[1], current[1])
		structureDistance = getStructureDistance(corr[0], current[0])
		if(distance < maxDistance and colorDistance < maxColorDistance and structureDistance < maxStructureDistance):
			plane.append(corr)
	plane.append(current)
	return plane

def findNormalChange(current, maxDistance, maxColorDistance, maxStructureDistance):
	plane = []
	totalAngle = 0
	for corr in correspondance:
		distance = getDistance(corr[0].getPosition(), current[0].getPosition())
		colorDistance = getColorDistance(corr[1], current[1])
		structureDistance = getStructureDistance(corr[0], current[0])
		if(distance < maxDistance and colorDistance < maxColorDistance and structureDistance < maxStructureDistance):
			plane.append(corr)
	for p in plane:
		
		angle = math.fabs(angle_between(p[2], current[2]))
		print angle
		totalAngle = totalAngle + angle
	return totalAngle / float(len(plane))

def calculateNormal(plane):
	pointArray = [p[0].point.getPosition() for p in plane]
	x = np.array(pointArray)
	n = len(pointArray)
	mean = np.mean(x, axis=0)
	h = np.array(list(range(1, n+1)))[:,None]
	B = x - np.multiply(h, mean)
	cov = (1./(n-1.)) *np.dot(np.transpose(np.conjugate(B)), B)
	w, v = np.linalg.eig(np.array(cov))
	eigenvalues = w.tolist()
	vectorIndex = eigenvalues.index(min(eigenvalues))
	center = [x for x in pointArray[0]]
	center.extend(v[vectorIndex].tolist())
	# print w, v
	return center

frame = np.zeros_like(image)
frame = cv2.add(image, frame)

plyLines = ""
normalArray = []
totalPoints = 0
# for i in range(20):
# 	# plane = findNearbyObs(correspondance[getRandomInt(555, 555)],200, 10, 50)
# 	plane = findNearbyObs(correspondance[getRandomInt(0, ptNum-1)],400, 10, 100)
# 	if(len(plane) > 5):
# 		totalPoints = totalPoints + len(plane)
# 		color = getRandomColor()
# 		print i
# 		normal = calculateNormal(plane)
# 		normalArray.append(normal)
# 		for p in plane:
# 			# draw on image
# 			u = int(p[0].getPosition()[0] + IMAGE_WIDTH / 2)
# 			v = int(p[0].getPosition()[1] + IMAGE_HEIGHT / 2)
# 			# cv2.circle(frame,(u, v),5, color,-1)
# 			# cv2.putText(frame, str(i), (u, v), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 0.5, (255, 255, 255), 2)
# 			# draw on ply
# 			plyLines = plyLines + ' '.join(map(str,p[0].point.getPosition())) + " " + ' '.join(map(str,normal[-3:])) + " " + ' '.join(map(str,[color[2], color[1], color[0]])) + "\n"


for c in correspondance:
	normalChange = findNormalChange(c, 400, 255, 100);
	print normalChange
	if(normalChange > 1.5):
		u = int(c[0].getPosition()[0] + IMAGE_WIDTH / 2)
		v = int(c[0].getPosition()[1] + IMAGE_HEIGHT / 2)
		cv2.circle(frame,(u, v),5, (0, 0, 255),-1)

# plane = findNearbyObs(correspondance[100], 500, 50)

# for p in plane:
# 	u = int(p[0].getPosition()[0] + IMAGE_WIDTH / 2)
# 	v = int(p[0].getPosition()[1] + IMAGE_HEIGHT / 2)
# 	cv2.circle(frame,(u, v),5, (255,255,255),-1)

cv2.imwrite("mask.jpg", frame)

# plyHeader = "ply\nformat ascii 1.0\nelement vertex " + str(totalPoints) + "\nproperty float x\nproperty float y\nproperty float z\nproperty float nx\nproperty float ny\nproperty float nz\nproperty uchar red\nproperty uchar green\nproperty uchar blue\nend_header\n"


# # for n in normalArray:
# # 	plyLines = plyLines + ' '.join(map(str,n)) + "\n"

# content = plyHeader + plyLines

# f = open('mask.ply', 'w')
# f.write(content)
# f.close()


# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
# import numpy as np
# print normalArray
# soa =np.array(normalArray) 

# X,Y,Z,U,V,W = zip(*soa)
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.quiver(X,Y,Z,U,V,W)
# ax.set_xlim([-20,30])
# ax.set_ylim([-20,30])
# ax.set_zlim([0,60])
# plt.show()