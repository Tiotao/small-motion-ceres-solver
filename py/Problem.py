from Observation import Observation
from Camera import Camera
from Point import Point

class Problem:

	def __init__(self):
		self.cam = []
		self.obs = []
		self.pts = []
		Point.setIdCount(0)
		Camera.setIdCount(0)
		Observation.setIdCount(0)
		# print Camera.count(), Point.count(), OBservation.count()

	def write(self, filename):
		content = self.toString()
		f = open(filename, 'w')
		f.write(content)
		f.close()

	def load(self, filename):
		with open(filename, 'r') as f:
			content = f.readlines()
		meta = content[0].split()
		camCount = int(meta[0])
		pointCount = int(meta[1])
		observationCount = int(meta[2])
		assert camCount * pointCount == observationCount
		camStartLine = observationCount + 1
		pointStartLine = camStartLine + 9 * camCount
		pointEndLine = pointStartLine + 3 * pointCount
		# add all cameras
		for i in xrange(camStartLine, pointStartLine, 9):
			rot = [float(content[i]), float(content[i+1]), float(content[i+2])]
			trans = [float(content[i+3]), float(content[i+4]), float(content[i+5])]
			f = float(content[i+6])
			k1 = float(content[i+7])
			k2 = float(content[i+8])
			c = Camera(rot, trans, f, k1, k2)
			self.cam.append(c)
		# all all points
		for j in xrange(pointStartLine, pointEndLine, 3):
			x = float(content[j])
			y = float(content[j+1])
			z = float(content[j+2])
			p = Point([x, y, z])
			self.pts.append(p)
		# all observations
		for k in xrange(1, camStartLine):
			line = content[k].split()
			camId = int(line[0])
			ptId = int(line[1])
			camObject = self.cam[camId]
			pointObject = self.pts[ptId]
			u = float(line[2])
			v = float(line[3])
			o = Observation([u, v], camObject, pointObject)
			self.obs.append(o)

	def toString(self):
		string = "%r %r %r" %(Camera.count(), Point.count(), Observation.count())
		for o in self.obs:
			string = string + "\n" + o.toString()
		for c in self.cam:
			string = string + "\n" + c.toString()
		for p in self.pts:
			string = string + "\n" + p.toString()
		return string

	def updateId(self, itemCollection):
		for index, item in enumerate(itemCollection):
			item.setId(index)

	def removeObs(self, id):
		self.obs = [o for o in self.obs if o.getId() is not id]
		Observation.setIdCount(Observation.count() - 1)

	def removeObsWithCam(self, camId):
		self.obs = [o for o in self.obs if o.camera.getId() is not camId]
		Observation.setIdCount(len(self.obs))
		self.updateId(self.obs)

	def getCameraDepth(self):
		depth = float('-inf')
		for c in self.cam:
			d = c.getTranslation()[2]
			if d > depth:
				depth = d
		return depth

	def removeObsWithPt(self, ptId):
		self.obs = [o for o in self.obs if o.point.getId() is not ptId]
		# print len(self.obs)
		Observation.setIdCount(len(self.obs))
		self.updateId(self.obs)

	def removeInvalidPt(self):
		camDepth = self.getCameraDepth()
		print camDepth
		Point.deleteCount = 0
		removedPtsId = [p.getId() for p in self.pts if p.isNegativeDepth(camDepth)]
		Point.deleteCount = 0
		self.pts = [p for p in self.pts if p.isNegativeDepth(camDepth) is False]
		Point.setIdCount(len(self.pts))
		for id in removedPtsId:
			print "Point Id %r Removed" %(id)
			self.removeObsWithPt(id)
		self.updateId(self.pts)

	# def removeCam(self, id):
	# 	Camera.setIdCount(Camera.count() - 1)
	# 	self.cam.pop(id)
	# 	self.removeObsWithCam(id)

		











