class Point:

	ptIdCount = 0
	deleteCount = 0

	def __init__(self, position):
		self.x = position[0]
		self.y = position[1]
		self.z = position[2]
		self.id = Point.ptIdCount
		Point.ptIdCount = Point.ptIdCount + 1

	@staticmethod
	def count():
		return Point.ptIdCount

	@staticmethod
	def setIdCount(n):
		Point.ptIdCount = n

	def setId(self, id):
		self.id = id

	def getId(self):
		return self.id

	def getX(self):
		return self.x

	def getY(self):
		return self.y

	def getZ(self):
		return self.z

	def getPosition(self):
		return [self.x, self.y, self.z]

	def setX(self, x):
		self.x = x

	def setY(self, y):
		self.y = y

	def setZ(self, z):
		self.z = z

	def setPosition(self, position):
		self.setX(position[0])
		self.setY(position[1])
		self.setZ(position[2])

	def isNegativeDepth(self, cameraDepth):
		# if (self.getZ() < cameraDepth and Point.deleteCount < 50):
		# 	print "Invalid Point %r : %r / %r" %(self.getId(), self.getZ(), cameraDepth)
		# 	Point.deleteCount = Point.deleteCount + 1
		# 	return True
		# else:
		# 	return False
		return False


	def toString(self):
		string = "%r\n%r\n%r" %(self.x, self.y, self.z)
		return string