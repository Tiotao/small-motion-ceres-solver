import numpy as np
import math
import cv2
import cv
import os
from Problem import Problem

class NormalMap:
    def __init__(self, width, height):
        #(color, normal, location)
        self.width = width
        self.height = height
        self.map = [[[None, None, None] for x in range(width)] for x in range(height)]
        print len(self.map)
     
    def loadImage(self, imageFile, sparseFile, matchingFile):
        image = cv2.imread(imageFile)
        # add color
        for v, row in enumerate(image):
            for u, color in enumerate(row):
                self.map[v][u][0] = (color[2], color[0], color[1]);
                
        # add normal and location  
        with open(sparseFile, 'r') as f:
            content = f.readlines()[17:]
            normalArray = [map(float, c.split(' ')[3:6]) for c in content]
            locationArray = [map(float, c.split(' ')[0:3]) for c in content]
        
        problem = Problem()
        problem.load(matchingFile)
        ptNum = len(problem.pts)
        observation = problem.obs[:ptNum]
        for id, obs in enumerate(observation):
            u = int(obs.u + self.width / 2)
            v = int(obs.v + self.height / 2)
            self.map[v][u][1] = normalArray[id]
            self.map[v][u][2] = locationArray[id]
            print self.map[v][u]
        
        
        
            
        


a = NormalMap(1920, 1080)
a.loadImage('image.png', 'normal.ply', 'matching.txt')

     