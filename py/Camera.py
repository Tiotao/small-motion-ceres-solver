class Camera:

	camIdCount = 0

	def __init__(self, rot, trans, focus, k1, k2):
		self.id = Camera.camIdCount
		self.rot = rot
		self.trans = trans
		self.focus = focus
		self.k1 = k1
		self.k2 = k2
		Camera.camIdCount = Camera.camIdCount + 1

	@staticmethod
	def count():
		return Camera.camIdCount

	@staticmethod
	def setIdCount(n):
		Camera.camIdCount = n

	def getRotation(self):
		return self.rot

	def getTranslation(self):
		return self.trans

	def getFocus(self):
		return self.focus

	def getK1(self):
		return self.k1

	def getK2(self):
		return self.k2

	def getId(self):
		return self.id

	def setId(self, id):
		self.id = id

	def setRotation(self, rot):
		self.rot = rot

	def setTranslation(self, trans):
		self.trans = trans

	def setFocus(self, focus):
		self.focus = focus

	def setK1(self, k1):
		self.k1 = k1

	def setK2(self, k2):
		self.k2 = k2

	def toString(self):
		string = "%r\n%r\n%r\n%r\n%r\n%r\n%r\n%r\n%r" %(self.rot[0], self.rot[1], self.rot[2], \
			self.trans[0], self.trans[1], self.trans[2], self.focus, self.k1, self.k2)
		return string






