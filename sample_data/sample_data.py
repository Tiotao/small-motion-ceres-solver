# This script is used to generate test data.
# Usage: python sample_data.py
# Output format: output.txt file with following data format
# ============================================================
# <no. of view> <no. of featrues> <no. of view-feature pairs>
# <view 0> <feature 0> <feature 0 u (at that view)> <feature 0 v (at that view)>
# <view 0> <feature 1> <feature 1 u (at that view)> <feature 1 v (at that view)>
# ...
# <view m> <feature n> <feature n u (at that view)> <feature n v (at that view)>
# <view 0 camera rotation X (radian)>
# <view 0 camera rotation Y>
# <view 0 camera rotation Z>
# <view 0 camera translation X>
# <view 0 camera translation Y>
# <view 0 camera translation Z>
# <view 0 camera focal length>
# <view 0 camera K1 (0 default)>
# <view 0 camera K2 (0 default)>
# ...
# <view m camera K2>
# <feature 0 initial guess X>
# <feature 0 initial guess Y>
# <feature 0 initial guess Z>
# ...
# <feature n initial guess X>
# <feature n initial guess X>
# <feature n initial guess X>


import numpy as np
import math
import matplotlib.pyplot as plt


F = 100 										# Focal Length
B_U = 1													
B_V = 1
U_0 = 0											# Offset X										
V_0 = 0											# Offset Y			
CAMERA_COOR_X = [1, 0, 0]						# Camera X Axis 
CAMERA_COOR_Y = [0, 1, 0]						# Camera Y Axis
CAMERA_COOR_Z = [0, 0, 1]						# Camera Z Axis
TRUE_VALUE = True 								# Initial guess is the true value
CAMERAS = [	([0.0, 0.0, 1], [0, 0, 10]),		# camera positions (rotation, translation)
			([2, 0, 0], [1, 0, 1]),
			([3, 0, 0], [2, 0, 1]),
			([0, 1, 0], [0, 0, 1]),
			([0, 2, 0], [0, 1, 1]),
			([0, 3, 0], [0, 2, 1]),
			([0, 0, 1], [0, 0, 1]),
			([0, 0, 2], [0, 0, 1]),
			([0, 0, 3], [1, 1, 1]),
			([2, 2, 2], [0, 0, 1])]

CAMERA_COUNT = len(CAMERAS)
USE_ESTIMATE_ROT = True

output_array = []

# create data pts
def pts_set_2():
 
  def create_intermediate_points(pt1, pt2, granularity):
    new_pts = []
    vector = np.array([(x[0] - x[1]) for x in zip(pt1, pt2)])
    return [(np.array(pt2) + (vector * (float(i)/granularity))) for i in range(1, granularity)]
 
  pts = []
  granularity = 20
 
  # Create cube wireframe
  pts.extend([[-50, -50, 100], [50, -50, 100], [50, 50, 100], [-50, 50, 100], \
              [-50, -50, 150], [50, -50, 150], [50, 50, 150], [-50, 50, 150]])
 
  pts.extend(create_intermediate_points([-50, -50, 150], [50, -50, 150], granularity))
  pts.extend(create_intermediate_points([50, -50, 150], [50, 50, 150], granularity))
  pts.extend(create_intermediate_points([50, 50, 150], [-50, 50, 150], granularity))
  pts.extend(create_intermediate_points([-50, 50, 150], [-50, -50, 150], granularity))
 
  pts.extend(create_intermediate_points([-50, -50, 100], [50, -50, 100], granularity))
  pts.extend(create_intermediate_points([50, -50, 100], [50, 50, 100], granularity))
  pts.extend(create_intermediate_points([50, 50, 100], [-50, 50, 100], granularity))
  pts.extend(create_intermediate_points([-50, 50, 100], [-50, -50, 100], granularity))
 
  pts.extend(create_intermediate_points([50, 50, 150], [50, 50, 100], granularity))
  pts.extend(create_intermediate_points([50, -50, 150], [50, -50, 100], granularity))
  pts.extend(create_intermediate_points([-50, -50, 150], [-50, -50, 100], granularity))
  pts.extend(create_intermediate_points([-50, 50, 150], [-50, 50, 100], granularity))
 
  # Create triangle wireframe
  pts.extend([[-25, -25, 100], [25, -25, 100], [0, 25, 100]])
  pts.extend(create_intermediate_points([-25, -25, 100], [25, -25, 100], granularity))
  pts.extend(create_intermediate_points([25, -25, 100], [0, 25, 100], granularity))
  pts.extend(create_intermediate_points([0, 25, 100], [-25, -25, 100], granularity))
  total_feature = len(pts)
  return np.array(pts), total_feature
 
pts, total_feature= pts_set_2() # include more points to produce wireframe


# convert angle axis to rotation matrix
def angleAxisToRot(axis, theta):
    axis = np.asarray(axis)
    theta = np.asarray(theta)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2)
    b, c, d = -axis*math.sin(theta/2)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.matrix([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])

# create rotation matrix
def rotationMatrix(rot):
	x = angleAxisToRot(CAMERA_COOR_X, math.radians(rot[0]))
	y = angleAxisToRot(CAMERA_COOR_Y, math.radians(rot[1]))
	z = angleAxisToRot(CAMERA_COOR_Z, math.radians(rot[2]))
	rot = x * y * z
	return rot

# estimate rotation matrix with small motion assumption
def rotationMatrixEstimate(rot):
	x = math.radians(rot[0])
	y = math.radians(rot[1])
	z = math.radians(rot[2])
	return np.matrix([	[1.0, -z, y], 
						[z, 1.0, -x],
						[-y, x, 1.0]])

# calculate camera coordinate based on rotation matrix
def getCamCoor(rot):
	if (USE_ESTIMATE_ROT):
		rot_matrix = rotationMatrixEstimate(rot)
	else:
		rot_matrix = rotationMatrix(rot)
	# print rot_matrix
	camX = np.matrix(CAMERA_COOR_X).T
	camY = np.matrix(CAMERA_COOR_Y).T
	camZ = np.matrix(CAMERA_COOR_Z).T
	camX_rotated = rot_matrix * camX
	camY_rotated = rot_matrix * camY
	camZ_rotated = rot_matrix * camZ
	return camX_rotated, camY_rotated, camZ_rotated
	
# generate perspective view
def persView(ax, rot, trans, camera):
	tf = np.matrix(trans).T # simple translation
	camCoor = getCamCoor(rot) # rotation
	i_f = camCoor[0]
	j_f = camCoor[1]
	k_f = camCoor[2]
	index = 0
	for sp in pts:
		sp = np.matrix(sp).T 
		sp_tf_T = (sp - tf).T
		u = ((F * sp_tf_T * i_f) * B_U / (sp_tf_T * k_f)) + U_0
		v = ((F * sp_tf_T * j_f) * B_V / (sp_tf_T * k_f)) + V_0
		output_array.append(str(camera) + ' ' + str(index) + ' ' + str(u[0, 0]) + ' ' + str(v[0, 0]))
		index = index + 1
		ax.plot(u[0, 0], v[0, 0], 'b.')

	ax.axis([-80, 80, -80, 80])



def generateCamParam(camNum):
	for c in xrange(0,camNum):
		if (TRUE_VALUE):
			output_array.append(str(-math.radians(CAMERAS[c][0][0]))) # rotation X
			output_array.append(str(-math.radians(CAMERAS[c][0][1]))) # rotation Y
			output_array.append(str(-math.radians(CAMERAS[c][0][2]))) # rotation Z
			output_array.append(str(-CAMERAS[c][1][0])) # translation X
			output_array.append(str(-CAMERAS[c][1][1])) # translation Y
			output_array.append(str(-CAMERAS[c][1][2])) # translation Z
			output_array.append(str(F)) # focal
			output_array.append("0") # k1
			output_array.append("0") # k2
		else:
			output_array.append("0")
			output_array.append("0")
			output_array.append("0")
			output_array.append("0")
			output_array.append("0")
			output_array.append("0")
			output_array.append(str(F))
			output_array.append("0")
			output_array.append("0")

# initial guess based on (u, v) in the image
def generateInitValue(rot, trans):
	tf = np.matrix(trans).T # simple translation
	camCoor = getCamCoor(rot) # rotation
	i_f = camCoor[0]
	j_f = camCoor[1]
	k_f = camCoor[2]
	index = 0
	# print camera, tf
	for sp in pts:

		sp = np.matrix(sp).T 
		sp_tf_T = (sp - tf).T
		u = ((F * sp_tf_T * i_f) * B_U / (sp_tf_T * k_f)) + U_0
		v = ((F * sp_tf_T * j_f) * B_V / (sp_tf_T * k_f)) + V_0
		# print camera, index, u[0, 0], v[0, 0]
		from random import randint
		output_array.append(str(u[0, 0]))
		output_array.append(str(v[0, 0]))
		output_array.append(str(100))

# initial guess based on the true value
def generateRightValue():
	for p in pts:
		output_array.append(str(p[0]))
		output_array.append(str(p[1]))
		output_array.append(str(p[2]))
	return pts


# generate data point, plot image
def generateData():
	f, ((ax0, ax1, ax2), (ax3, ax4, ax5), (ax6, ax7, ax8), (ax9, ax10, ax11)) = plt.subplots(4, 3, sharex='col', sharey='row')
	axs = [ax0, ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11]
	# file header
	output_array.append(str(CAMERA_COUNT) + ' ' + str(total_feature) + ' ' + str(CAMERA_COUNT * total_feature))
	# observations
	for c in xrange(0,CAMERA_COUNT):
		persView(axs[c], CAMERAS[c][0], CAMERAS[c][1], c) # ROT, TRANS
	
	# camera parameters
	generateCamParam(CAMERA_COUNT)
	
	# Init value
	if(TRUE_VALUE):
		generateRightValue()
	else:
		generateInitValue(CAMERAS[0][0], CAMERAS[0][1])
	
	f = open('output.txt','w')
	for line in output_array[:-1]:
		f.write(line + '\n')
	f.write(output_array[-1])
	f.close()

	# plot view
	plt.savefig('camera_view.png')
	plt.clf()

generateData()

