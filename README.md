Small Motion Ceres Solver
=========================
# Changes

A few changes has been made:

* commented out Normalization step in `bundle_adjuster.cc (line 333)`
* Modified `snavely_reprojection_error.cc`. `Strut SnavelyReprojectionError` used to host error functions for BAL problems. Now I have replaced the error function with the one from Fisher Yu's paper. See line 60 onwards. 

# Build

please refers to 
[Ceres Solver Building](https://ceres-solver.org/building.html)

# Test Data

`sample_data.py` will create sample data for testing. See comments in the file for details.

# Run

```
ceres-bin/bin/bundle_adjuster --input=[filename.txt]

```

`filename.txt.ply` will be generated in `output`.

